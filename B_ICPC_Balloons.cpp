#include<bits/stdc++.h>
#include<map>
using namespace std;
int main()
{
    int t;
    cin >> t;
    while (t--)
    {
        int n;
        cin >> n;
        string s;
        cin >> s;
        map <char,int>m;
        char A='A';
        for(char i=1;i<=26;i++)
        {
            m.insert({A,0});
            A++;
        }
        for(int i=0;i<n;i++)
        {
            for(char j='A'; j<='z'; j++)
            {
                if(s[i]==j){
                    m[j]++;
                }
            }
        }

        // for(auto i: m){
        //     cout <<i.first <<" "<<i.second<<endl;
        // }

        int B=0;
        for(auto i: m){
            if(i.second>0){
                B+=1;
                B+=i.second;
            }
        }
        cout<<B<<endl;
    }
    
    return 0;
}